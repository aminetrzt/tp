const express = require("express");
const app = express();
const fichier = require("./fichier.json");
app.set("view engine", "ejs");
var fs = require("fs");

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }))


/*Créer un serveur web donnant accès une page d’accueil avec des informations sur l’application.
 La page sera accessible via l’url    127.0.0.1:3000/  */

app.get("/", (req, res) => {
  res.render("home");
});




/* 127.0.0.1:3000/contact : affiche des données contact (téléphone, email,…)
(En contenu html).*/ 
app.post("/contact", function(req, res) {
  var contct = {
    nom: req.body.nom,
    telephone:req.body.telephone ,
    email: req.body.email,
    adresse: req.body.adresse
  };
  res.render("contact", contct);
});




/*127.0.0.1:3000/persone : affiche une liste de personnes avec
 leurs informations à partir d’un fichier JSON existant
, en utilisant les vues express et la méthode GET. */
app.get("/persone", function(req, res) {
  res.render("persone", { fichier: fichier });
});




/*127.0.0.1:3000/persone/add/ :id/ :name : crée  un fichier JSON avec identifiant et 
un nom d’utilisateur(le nom du fichier JSON doit être l’id saisie dans l’URL),
  en utilisant et la méthode POST. */

// avec la methode GET

app.get("/persone/add/:id/:name", function(req, res) {
  console.log(req.params);
  let data = {
    id: req.params.id,
    name: req.params.name
  };

  fs.writeFileSync(req.params.id + ".json", JSON.stringify(data));
  res.render("home")
});

/*127.0.0.1:3000/persone/add/ :id/ :name : crée  un fichier JSON avec identifiant et 
un nom d’utilisateur(le nom du fichier JSON doit être l’id saisie dans l’URL),
  en utilisant et la méthode POST. */

// avec la methode POST

app.post("/persone/add", function(req, res) {
  console.log(req.body);
  let data = {
    id: req.body.id,
    name: req.body.name
  };

  fs.writeFileSync(req.body.id + ".json", JSON.stringify(data))
  res.render("home")
});

/*127.0.0.1:3000/persone/ :id: affiche les données d’une personnes
 à partir d’un fichier JSON existant,
 en utilisant les  vues express et la méthode GET. */

app.get("/persone/:id", function(req, res) {
  // let q = parseInt(req.params.id);
  let q = req.params.id;
  console.log(req.params);
  let r = null;
  for (const i in fichier) {
    const user = fichier[i];
    if (user.id === q) {
      r = user;
    }
  }

  if (r !== null) {
    res.render("personeid", r);
  } else {
    res.status(404).send("Sorry can't find that!");
  }
});





/*127.0.0.1:3000/persone/update/ :id/ :email : mettre à jour un fichier un fichier JSON existant 
 en utilisant l’ id qui est le nom du fichier,
 en ajoutant la données email aux informations existantes à l’intérieurs du fichier. */

app.get("/persone/update/:id/:email", function(req, res) {
   let namefile = req.params.id+'.json'
  
   fs.readFile(namefile, (err, data) => {
    if (err) throw err;
    //json to js objcet 
    var update = JSON.parse(data);
    //ajouter l email dans la variable update 
    update.email= req.params.email
    console.log(update);
    //js  to  json
    let update2 = JSON.stringify(update, null, 2);
    //ecrire les donnees dans le fichier 
    fs.writeFile(namefile, update2 , err => {
      if (err) throw err;
      console.log("Data written to file");
    });
  });

  res.send("ok");
});



app.use(function(req, res, next) {
  next(res.status(404).render("erreur"));
});

app.listen(3000, function() {
  console.log("Example app listening on port 3000!")
});
